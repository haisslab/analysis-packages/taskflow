const output = document.getElementById("output");
const startJobButton = document.getElementById("startJobButton");

$(document).ready(function() {
    var jobList = $('#jobList');
    var url = 'https://gitlab.pasteur.fr/api/v4/projects/haisslab%2Fanalysis-packages%2Ftaskflow_jobs/repository/tree/?recursive=true&per_page=100';
    $.ajax({
      url: url,
      dataType: 'json',
    }).done(function(data) {
      for (var i = 0; i < data.length; i++) {
        var file = data[i];
        console.log(file)
        if (file.type == "blob" && file.name.endsWith('.py')) {
          var jobName = file.name.substring(0, file.name.length - 3);
          jobList.append($('<option>', {
            value: jobName,
            text: jobName
          }));
        }
      }
    });
  });
  

class EventSourceConnexionHandler {
    constructor(eventsource, maxReconnectTries = 3) {
      this.reconnectAttempts = 0;
      this.maxReconnectTries = maxReconnectTries;
      this.eventsource = eventsource;
    }
    parse_message(event){
        console.log("Received a message from server:",event.data)
        var [type, data] = JSON.parse(event.data);
        console.log(type,data)
        if (type == 'close' || type == null){
            this.eventsource.close();
            return;
        }
        this.reconnectAttempts = 0;
        return data + "\n";
    }
    handle_reconnection(){
        if (this.reconnectAttempts > this.maxReconnectTries) {
            this.eventsource.close();
        } 
        else {
            this.reconnectAttempts++;
        }
    }
};

function startJob() {
    var args = document.getElementById("args").value;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200){
            var response = JSON.parse(this.responseText);
            var redirect_url = response.redirect_url;
            console.log("Stream redirect url:", redirect_url);
            var taskId = response.task_id;
            var eventSource = new EventSource(redirect_url);
            var connexion_handler = new EventSourceConnexionHandler(eventSource,3);
            eventSource.onmessage = (event) => appendLog(event,connexion_handler);
            eventSource.onerror = () => connexion_handler.handle_reconnection()
            addOutputLine("Job started\n");
        }
    };
    xhr.open("POST", "/run_task?args=" + encodeURIComponent(args));
    xhr.send();
};

function appendLog(event,connexion_handler) {
    message = connexion_handler.parse_message(event)
    if (message == null){
        console.log("Received a close message from server:");
        message = "Job finished\n\n";
    }
    addOutputLine(message)
};

function addOutputLine(message){
    var streamingOutput = document.getElementById("streamingOutput");
    var logLine = document.createElement('div');
    logLine.classList.add('log-line');
    var timestamp = document.createElement('div');
    timestamp.classList.add('timestamp');
    timestamp.textContent = addTimestamp();
    logLine.appendChild(timestamp);
    var logMessage = document.createElement('div');
    logMessage.classList.add('message');
    logMessage.textContent = message;
    logLine.appendChild(logMessage);
    streamingOutput.appendChild(logLine);
  };
    
  function addTimestamp() {
    const now = new Date();
    return '[' + [now.getHours(), now.getMinutes(), now.getSeconds()].join(':') + ']';
  };

// function addOutputLine(message){
//     var streamingOutput = document.getElementById("streamingOutput");
//     streamingOutput.innerHTML += `<div class="log-line">${addTimestamp() + message}</div>`;
// };

// function addTimestamp() {
//     const now = new Date();
//     return '[' + [now.getHours(), now.getMinutes(), now.getSeconds()].join(':') + '] ';
// };