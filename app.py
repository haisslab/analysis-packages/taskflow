#%%
import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        host='localhost',
        port=5672,
        credentials=pika.PlainCredentials('guest', 'guest')
    )
)
channel = connection.channel()

# Test your connection here

connection.close()


#%%
from flask import (Flask, 
                    jsonify, 
                    render_template, 
                    request, Response, 
                    stream_with_context, 
                    send_from_directory,
                    redirect)
from celery import Celery
from celery.result import AsyncResult
import pika
import time

app = Flask(__name__, template_folder='templates', static_folder='static')
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'  #'redis://localhost:6379/0'
#app.config['CELERY_RESULT_BACKEND'] = 'rpc://'  #'redis://localhost:6379/0'
app.config['CELERYD_CONCURRENCY'] = 2  # set the number of workers to 2 maximum

celery_app = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery_app.conf.update(app.config)

# initialize RabbitMQ
params = pika.ConnectionParameters(host='localhost', port=5672)
connection = pika.BlockingConnection(params)
channel = connection.channel()

class LogMonitor:
    def __init__(self, log_path, last_pos=0):
        self.log_path = log_path
        self.last_pos = last_pos

    def get_new_lines(self):
        with open(self.log_path, "r") as f:
            f.seek(self.last_pos)
            new_lines = f.readlines()
            self.last_pos = f.tell()
        return new_lines

def streamify_message(message : str, header = "message"):
    import json
    return "data:"+json.dumps([header,message])+"\n\n"

@app.route('/favicon.ico', methods=['GET'])
def favicon():
    import os
    favicon_path = os.path.join(app.root_path, 'static')
    print(favicon_path)
    return send_from_directory(favicon_path,'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/run_task', methods=['POST'])
def run_task():
    args = request.args.get('args', default='').split(",")
    print(args)

    # create log file
    log_path = "task.log"
    with open(log_path, "w") as f:
        pass

    # start task with writing its output to a log file
    result = long_running_task.apply_async(args=[args, log_path])

    # Redirect to stream_task route with task_id
    return jsonify(task_id=result.id, redirect_url='/stream_task/{}'.format(result.id))

@celery_app.task(bind=True)
def long_running_task(self, args, log_path):
    import time
    for i in range(10):
        with open(log_path, "a") as f:
            f.write("task {}\n".format(i))
        time.sleep(1)
    self.update_state(state='SUCCESS')
    return

@app.route('/stream_task/<task_id>')
def stream_task(task_id):
    print("mais")
    def generate():
        work = AsyncResult(task_id, app=celery_app)
        
        log_path = "task.log"
        log_monitor = LogMonitor(log_path)
        
        if not work:
            print("task not found")
            yield streamify_message("Task Not Found", "close")
             #return
        else :
            try:
                new_lines = log_monitor.get_new_lines()
                for line in new_lines:
                    yield streamify_message(line)
            except:
                yield streamify_message("Error in stream_task", "close")
        
    return Response(stream_with_context(generate()), mimetype='text/event-stream')

@app.route('/task_status/<task_id>')
def task_status(task_id):
    task_result = AsyncResult(task_id, app=celery)
    if task_result.successful():
        # Return result
        return jsonify(result=task_result.result)
    elif task_result.failed():
        # Task failed
        return jsonify(error='Task failed')
    else:
        # Task still in progress
        return jsonify(status='Task in progress')
    
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/view_job/<task_id>')
def rich_edit_session(session_id):
    print(session_id)
    return render_template('index.html')
