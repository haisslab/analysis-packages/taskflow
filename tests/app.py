from flask import Flask, render_template, request, Response
from celery import Celery
from subprocess import Popen, PIPE, STDOUT, CalledProcessError

app = Flask(__name__, template_folder='templates', static_folder='static')
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/run_job', methods=['GET'])
def run_job():
    args = request.args.get('args', default='')
    
    def run_job_async(args):
        msg = "Starting new job"
        print(msg)
        yield msg
        msg = "Arguments : " + str(args)
        print(msg)
        yield msg
        try :
            process = Popen(['python', 'test.py'] + args.split(), stdout=PIPE, stderr=STDOUT, bufsize=1, universal_newlines=True)
            while True :
                output = process.stdout.readline()
                if output == '' and process.poll() is not None:
                    break
                if output :
                    msg = 'data: {}\n'.format(output.strip())
                    print(msg)
                    yield msg
            process.wait()
            if process.returncode != 0:
                raise CalledProcessError(process.returncode, process.args)
        except CalledProcessError as e:
            msg = 'data: An error occurred inside the job: {}\n'.format(e)
            print(msg)
            yield msg
        except Exception as e:
            msg = 'data: An error occurred in the job wrapper: {}\n'.format(e)
            print(msg)
            yield msg
        finally :
            msg = "Job closed"
            print(msg)
            yield msg
    return Response(run_job_async(args), mimetype='text/event-stream')

if __name__ == '__main__':
    app.run(debug=True)